﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace PhysicsSimulator.entity.UI
{
    public class Camera
    {
        public static Vector2 Position { get; set; } = new Vector2(0, 0);
        public static float Scale { get; set; } = 1;
        private readonly float MAXSCALECHANGE = 0.001f;
        private readonly float MAXPOSMOVE = 100;

        public void Update(List<Vector2> positions)
        {
            Position=CalculatePosition(positions);
            Scale = CalculateScale(positions);
        }

        public Vector2 SpritePosition(Vector2 pos)
        {
            return (pos - Position)*Scale + new Vector2(WindowSize.Width / 2, WindowSize.Height / 2);
        }

        private Vector2 CalculatePosition(List<Vector2> positions)
        {
            Vector2 sum = Vector2.Zero;
            foreach (Vector2 pos in positions)
            {
                sum += pos;
            }
            sum = sum / positions.Count;
            Vector2 dP = sum - Position;
            if (dP.Length() > MAXPOSMOVE) {
                dP.Normalize();
                return Position + dP*MAXPOSMOVE;
            }
            return sum;
        }

        private float CalculateScale(List<Vector2> positions)
        {
            //if (positions.Count < 2)
            //   return 1.5f;


            /**
            float desiredScale = 0.2f;
            float smallestY = float.MaxValue;
            float largestY = float.MinValue;
            foreach(Vector2 pos in positions)
            {
                if (pos.Y > largestY)
                    largestY = pos.Y;

                if (pos.Y < smallestY)
                    smallestY = pos.Y;
            }
            return (float)Math.Pow(desiredScale*WindowSize.Height/(largestY-smallestY),0.1);
            */
            int k = 0;
            int tempCount = positions.Count;
            while (tempCount > 6 * k)
            {
                tempCount -= 6 * k;
                k++;
            }

            float desiredScale = 1/(float)Math.Pow(1.2,k-1+(tempCount-1)/k/6);
            float heightDiffSum = 0;
            foreach (Vector2 pos1 in positions)
            {
                foreach (Vector2 pos2 in positions)
                {
                    if (pos1 != pos2)
                        heightDiffSum += Math.Abs(pos1.Y - pos2.Y);
                }
            }
            float scale = (float)Math.Pow(desiredScale * WindowSize.Height / (600*Math.Pow(positions.Count, 1 / 2)),1);///(heightDiffSum / (positions.Count* (positions.Count-1))), 1);

            if (scale - Scale < -MAXSCALECHANGE)
                scale = Scale - MAXSCALECHANGE;
            else if (scale - Scale > MAXSCALECHANGE)
                scale = Scale + MAXSCALECHANGE;
            return scale;
        }
    }
}