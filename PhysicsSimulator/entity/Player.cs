﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PhysicsSimulator.entity.controller;
using PhysicsSimulator.entity.controller.input;
using PhysicsSimulator.entity.UI;
using PhysicsSimulator.objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhysicsSimulator.entity
{
    class Player : MoleculeEntity
    {
        public Camera Camera { get; }


        public Player(Molecule molecule) : base(molecule)
        {
            Controls = new PlayerController();
            Camera = new Camera();
        }

        public override void Update(GameTime gameTime, List<MoleculeEntity> players)
        {
            UpdateCamera();
            Command = Controls.GetCommand(Position);
            foreach (Molecule m in Molecules)
                m.Command = Command;
            base.Update(gameTime, players);
            InputHandler.UpdatePreviousState();
        }
        private void UpdateCamera()
        {
            List<Vector2> positions = new List<Vector2>();
            foreach (Molecule m in Molecules)
                positions.Add(m.Position);
            Camera.Update(positions);
        }

        public void Draw(SpriteBatch sb, GameTime gameTime, List<MoleculeEntity> players)
        {
            List<Molecule> molecules = new List<Molecule>();
            foreach (MoleculeEntity p in players)
                molecules.AddRange(p.Molecules.ToArray());
            foreach (Molecule molecule in molecules)
            {
                molecule.Draw(sb, gameTime, Camera);
            }
        }
    }
}
