﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhysicsSimulator.entity.controller.input
{
    public enum MouseButton
    {
        LEFT,
        RIGHT,
        MIDDLE
    }
}
