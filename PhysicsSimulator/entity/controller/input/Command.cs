﻿using Microsoft.Xna.Framework;

namespace PhysicsSimulator.entity.controller.input
{
    public class Command
    {
        public float mPosX { get; private set; }
        public float mPosY { get; private set; }
        public double TAngle { get; private set; }
        public bool Thrust { get; private set; }

        public Command(bool thrust, double tAngle, float mPosX, float mPosY)
        {
            this.Thrust = thrust;
            this.TAngle = tAngle;
            //this.mPosX = mPosX+pos.X;
            //this.mPosY = mPosY+pos.Y;
            this.mPosX = mPosX;
            this.mPosY = mPosY;
        }
    }
}