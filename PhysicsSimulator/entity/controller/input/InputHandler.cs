﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using PhysicsSimulator.entity.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhysicsSimulator.entity.controller.input
{ 
    public static class InputHandler
    {
        public static Vector2 mPosition { get { return (new Vector2(Mouse.GetState().Position.X, Mouse.GetState().Position.Y) - new Vector2(WindowSize.Width / 2, WindowSize.Height / 2))/Camera.Scale; } }
        static KeyboardState kState;
        static KeyboardState pKState;
        static MouseState mState;
        static MouseState pMState;


        static InputHandler()
        {
            kState = Keyboard.GetState();
            pKState = kState;
            mState = Mouse.GetState();
            pMState = mState;
        }

        /// <summaryTo
        /// > be ran at the end/start of the Update-method in Game1
        /// </summary>
        public static void UpdatePreviousState()
        {
            pKState = kState;
            kState = Keyboard.GetState();
            pMState = mState;
            mState = Mouse.GetState();
        }

        public static bool IsPressed(Keys key)
        {
            return kState.IsKeyDown(key);
        }

        public static bool IsReleased(Keys key)
        {
            return kState.IsKeyUp(key);
        }

        public static bool IsJustPressed(Keys key)
        {
            return kState.IsKeyDown(key) && !pKState.IsKeyDown(key);
        }

        public static bool IsJustReleased(Keys key)
        {
            return kState.IsKeyUp(key) && !pKState.IsKeyUp(key);
        }

        public static bool IsPressed(MouseButton mb)
        {
            switch (mb)
            {
                case MouseButton.LEFT:
                    return mState.LeftButton == ButtonState.Pressed;
                case MouseButton.RIGHT:
                    return mState.RightButton == ButtonState.Pressed;
                case MouseButton.MIDDLE:
                    return mState.MiddleButton == ButtonState.Pressed;
                default:
                    throw new NotImplementedException();
            }
        }

        public static bool IsReleased(MouseButton mb)
        {
            switch (mb)
            {
                case MouseButton.LEFT:
                    return mState.LeftButton == ButtonState.Released;
                case MouseButton.RIGHT:
                    return mState.RightButton == ButtonState.Released;
                case MouseButton.MIDDLE:
                    return mState.MiddleButton == ButtonState.Released;
                default:
                    throw new NotImplementedException();
            }
        }

        public static bool IsJustPressed(MouseButton mb)
        {
            switch (mb)
            {
                case MouseButton.LEFT:
                    return mState.LeftButton == ButtonState.Pressed && pMState.LeftButton != ButtonState.Pressed;
                case MouseButton.RIGHT:
                    return mState.RightButton == ButtonState.Pressed && pMState.RightButton != ButtonState.Pressed;
                case MouseButton.MIDDLE:
                    return mState.MiddleButton == ButtonState.Pressed && pMState.MiddleButton != ButtonState.Pressed;
                default:
                    throw new NotImplementedException();
            }
        }

        public static bool IsJustReleased(MouseButton mb)
        {
            switch (mb)
            {
                case MouseButton.LEFT:
                    return mState.LeftButton == ButtonState.Released && pMState.LeftButton != ButtonState.Released;
                case MouseButton.RIGHT:
                    return mState.RightButton == ButtonState.Released && pMState.LeftButton != ButtonState.Released;
                case MouseButton.MIDDLE:
                    return mState.MiddleButton == ButtonState.Released && pMState.LeftButton != ButtonState.Released;
                default:
                    throw new NotImplementedException();
            }
        }
    }
}
