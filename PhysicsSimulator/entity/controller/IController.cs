﻿using Microsoft.Xna.Framework;
using PhysicsSimulator.entity.controller.input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhysicsSimulator.entity.controller
{
    interface IController
    {
        Command GetCommand(Vector2 position);
    }
}
