﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using PhysicsSimulator.entity.controller.input;

namespace PhysicsSimulator.entity.controller
{
    internal class PlayerController : IController
    {
        public PlayerController()
        {
        }
        public Command GetCommand(Vector2 position)
        {
            bool w = InputHandler.IsPressed(Keys.W);
            bool a = InputHandler.IsPressed(Keys.A);
            bool s = InputHandler.IsPressed(Keys.S);
            bool d = InputHandler.IsPressed(Keys.D);
            bool thrust = w || a || s || d;
            double tAngle = CalculateAngle(w,a,s,d);
            //float dx = InputHandler.mPosition.X - position.X;
            //float dy = InputHandler.mPosition.Y - position.Y;
            Command command = new Command(thrust, tAngle, InputHandler.mPosition.X+position.X, InputHandler.mPosition.Y+position.Y);
            return command;
        }

        private double CalculateAngle(bool w, bool a, bool s, bool d)
        {
            if(w && !s)
            {
                if (a && !d)
                {
                    return -3 * Math.PI / 4;
                }
                else if (!a && d)
                {
                    return -Math.PI / 4;
                }
                else
                    return -Math.PI / 2;
            }
            else if (!w && s)
            {
                if (a && !d)
                {
                    return -Math.PI * 5 / 4;
                }
                else if (!a && d)
                {
                    return -Math.PI * 7 / 4;
                }
                else
                    return -Math.PI * 3 / 2;
            }
            else if (a && !d)
            {
                return Math.PI;
            }
            return 0;
        }
    }
}