﻿using PhysicsSimulator.objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhysicsSimulator.entity
{
    class Neutral : MoleculeEntity
    {
        public Neutral(Molecule molecule) : base(molecule)
        {
        }
    }
}
