﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PhysicsSimulator.entity;
using PhysicsSimulator.entity.controller;
using PhysicsSimulator.entity.controller.input;
using PhysicsSimulator.entity.UI;
using PhysicsSimulator.objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhysicsSimulator.entity
{
    
    abstract class MoleculeEntity
    {
        public List<Molecule> Molecules { get; set; }
        protected IController Controls { get; set; }
        protected Command Command { get; set; }
        protected Color Color { get; set; }
        public Vector2 Position
        {
            get
            {
                List<Vector2> positions = new List<Vector2>();
                foreach (Molecule m in Molecules)
                    positions.Add(m.Position);
                Vector2 sum = Vector2.Zero;
                foreach (Vector2 pos in positions)
                {
                    sum += pos;
                }
                sum = sum / positions.Count;
                return sum;
            }
        }

        public MoleculeEntity(Molecule molecule)
        {
            Molecules = new List<Molecule>();
            Molecules.Add(molecule);
        }

        public virtual void Update(GameTime gameTime, List<MoleculeEntity> players)
        {         
            List<Molecule> interactingMolecules = new List<Molecule>();
            foreach (MoleculeEntity p in players)
            {
                if (p != this) {
                    if (Vector2.Distance(p.Position, Position) > 2000 || !(p is Player))
                    {
                        interactingMolecules.AddRange(p.Molecules.ToArray());
                    }
                }
            }
            interactingMolecules.AddRange(Molecules.ToArray());
            foreach (Molecule molecule in Molecules)
            {
                molecule.Update(gameTime, interactingMolecules.ToArray());
            }
            foreach (Molecule molecule in Molecules)
            {
                molecule.Move();
            }
            
        }
    }
}
