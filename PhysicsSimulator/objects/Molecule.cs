﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using PhysicsSimulator.entity;
using PhysicsSimulator.entity.controller.input;
using PhysicsSimulator.entity.UI;
using System;
using System.Collections.Generic;

namespace PhysicsSimulator.objects
{
    class Molecule
    {
        public virtual Vector2 Position { get { return Sprite.Position; } set { Sprite.Position = value; } }
        public Sprite Sprite { get; set; }
        protected float Angle { get { return Sprite.Rotation; } set { Sprite.Rotation = value; } }
        protected IDs id;
        public virtual float Friction { get; set; }
        private Vector2 FrictionForce { get { return Friction * Velocity / 100; } }
        public Vector2 Velocity { get; set; } = Vector2.Zero; //-!
        public Command Command { get; set; }

        protected virtual float TurnSpeed { get; set; }
        public virtual float Mass { get; set; }
        private Vector2 Acceleration { get { return (TotalExteriorForce - FrictionForce) / Mass; } }
        private Vector2 TotalExteriorForce { get; set; }
        private bool Connectable { get; set; } = true;
        public float Attraction { get { return attraction * Mass; } set{ attraction = value / Mass; } }
        private float attraction;
        public float Repulsion { get; set; }

        public Molecule(Vector2 position, IDs id = IDs.DEFAULT) // : base(sprite.spriteRect.Width, sprite.spriteRect.Height)
        {
            if (id == IDs.DEFAULT)
                id = EntityConstants.TypeToID(GetType());
            this.id = id;
            SetStats(id);
            Position = position;
        }

        public void Update(GameTime gameTime, Molecule[] molecules)
        {
            ProcessCommand();
            foreach(Molecule m in molecules)
            {
                double r = Vector2.Distance(m.Position, Position);
                if (r < 10000 && !m.Equals(this))
                {
                    if (r < 10)
                        r = 10;
                    //float res = (float)(attraction / Math.Pow(r, 1) - repulsion / Math.Pow(r, 2)); //force calculation
                    float res = (float)(m.Attraction*Attraction / Math.Pow(r, 1) - m.Repulsion*Repulsion / Math.Pow(r, 2)); //force calculation
                    //if (Mass * m.Mass*(repulsion - attraction) > 1000)
                        //res = 1000;
                    AddForce(res*Vector2.Normalize(m.Position - Position));
                }
            }
        }

        private void ProcessCommand()
        {
            if (Command != null)
            {
                CalculateAngle(Command.mPosX, Command.mPosY);
                if (Command.Thrust)
                    AddForce(EntityConstants.GetStatsFromID(EntityConstants.THRUST, id), (float)Command.TAngle);
                Command = null;
            }
        }

        public bool Connect(Vector2 pos, float attraction)
        {
            if (Connectable && Vector2.Distance(pos, Position) < 100*Math.Pow(attraction,1/2))
            {
                Connectable = false;
                return true;
            }
            return false;
        }

        public  void SetStats(IDs id)
        {
            Friction = EntityConstants.GetStatsFromID(EntityConstants.FRICTION, id);
            TurnSpeed = EntityConstants.GetStatsFromID(EntityConstants.TURNSPEED, id);
            Mass = EntityConstants.GetStatsFromID(EntityConstants.MASS, id);
            Sprite = SpriteHandler.GetSprite((int)id);
            Sprite.Origin = new Vector2(Sprite.SpriteRect.Width / 2, Sprite.SpriteRect.Height / 2);
            attraction = (float)EntityConstants.GetStatsFromID(EntityConstants.ATTRACTION, id);
            Repulsion = (float)EntityConstants.GetStatsFromID(EntityConstants.REPULSION, id);
        }

        public virtual void Stop()
        {
            Velocity = new Vector2(0, 0);
            TotalExteriorForce = new Vector2(0, 0);
        }

        public virtual void AddSpeed(float speed, float angle){ AddForce(new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle)) * speed * Mass); }

        public virtual void AddForce(Vector2 appliedForce) { TotalExteriorForce += appliedForce; }

        public virtual void AddForce(float force, float angle) { AddForce(force * (new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle)))); }

        public virtual void CalculateAngle(float mPosX, float mPosY)
        {
            float dX = mPosX - Position.X;
            float dY = mPosY - Position.Y;
            float addedAngle = 0;
            if (dX == 0)
            {
                dX = 0.00001f;
            }
            addedAngle = (float)Math.Atan(dY / dX);
            if (dX > 0)
                addedAngle += (float)Math.PI;

            if (Math.Abs(addedAngle - Angle) < Math.Abs(addedAngle - Angle + (2 * (float)Math.PI)))
                addedAngle = addedAngle - Angle;
            else
                addedAngle = addedAngle - Angle + (2 * (float)Math.PI);

            if (addedAngle > TurnSpeed)
            {
                addedAngle = TurnSpeed;
            }
            else if (addedAngle < -TurnSpeed)
            {
                addedAngle = -TurnSpeed;
            }
            Angle += addedAngle;
            if (Angle < 0)
                Angle += (2 * (float)Math.PI);
            Angle = Angle % (2 * (float)Math.PI);
        }

        public virtual void Move()
        {
            Vector2 Acceleration = this.Acceleration;
            //if (Acceleration.Length() > 200)
            //{
            //    Acceleration.Normalize();
            //    Acceleration *= 200;
            //}
            Velocity += Acceleration;
            //if (Velocity.Length() > 30)
            //{
            //    Velocity.Normalize();
            //    Velocity *= (float)Math.Sqrt(30);
            //}
                
                    

            Position += Velocity;
            TotalExteriorForce = new Vector2(0, 0);
        }

        public virtual void Draw(SpriteBatch spriteBatch, GameTime gameTime, Camera camera)
        {
            Sprite.Draw(spriteBatch, gameTime, camera);
        }
    }
}