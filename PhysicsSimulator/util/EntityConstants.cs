﻿using PhysicsSimulator;
using PhysicsSimulator.objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhysicsSimulator
{
    class EntityConstants
    {
        public static IDs TypeToID(Type t)
        {
            while (!TYPETOID.Keys.Contains(t))
            {
                if (t == typeof(object))
                    return IDs.DEFAULT;
                t = t.BaseType;
            }
            return TYPETOID[t];
        }

        public static readonly Dictionary<Type, IDs> TYPETOID =
            new Dictionary<Type, IDs>
            {
               {typeof(Molecule), IDs.MOLECULE},

            };

        public static readonly Dictionary<IDs, Type> IDTOTYPE = ReverseDic(TYPETOID);

        private static Dictionary<TValue, TKey> ReverseDic<TKey, TValue>(Dictionary<TKey, TValue> source)
        {
            var dictionary = new Dictionary<TValue, TKey>();
            foreach (var entry in source)
            {
                if (!dictionary.ContainsKey(entry.Value))
                    dictionary.Add(entry.Value, entry.Key);
            }
            return dictionary;
        }

        public static T GetStatsFromID<T>(Dictionary<int, T> dic, IDs id)
        {
            while (!dic.Keys.Contains((int)id))
            {
                if (!IDTOTYPE.Keys.Contains(id))
                    return dic[(int)IDs.DEFAULT];
                id = TypeToID(IDTOTYPE[id].BaseType);
            }
            return dic[(int)id];
        }

        public static readonly Dictionary<int, float> HEALTH =
          new Dictionary<int, float>
          {
                {(int)IDs.DEFAULT, 1},
                {(int)IDs.MOLECULE, 1}

          };

        public static readonly Dictionary<int, float> DAMAGE =
          new Dictionary<int, float>
          {
                {(int)IDs.DEFAULT, 1},
                {(int)IDs.MOLECULE, 1}
          };

        public static readonly Dictionary<int, float> MASS =
          new Dictionary<int, float>
          {
                {(int)IDs.DEFAULT, 10},
                {(int)IDs.MOLECULE, 10}
          };

        public static readonly Dictionary<int, float> THRUST =
         new Dictionary<int, float>
         {
                {(int)IDs.DEFAULT, 10},
                {(int)IDs.MOLECULE, 10}
         };

        public static readonly Dictionary<int, float> TURNSPEED =
        new Dictionary<int, float>
        {
                {(int)IDs.DEFAULT, 1000f * (float)Math.PI },
                {(int)IDs.MOLECULE, 1000f * (float)Math.PI }
        };

        public static readonly Dictionary<int, float> FRICTION =
          new Dictionary<int, float>
          {
                {(int)IDs.DEFAULT, 100},
                {(int)IDs.MOLECULE, 100}
          };

        public static readonly Dictionary<int, float> SCORE =
        new Dictionary<int, float>
        {
                {(int)IDs.DEFAULT, 1},
                {(int)IDs.MOLECULE, 1}
        };

        public static readonly Dictionary<int, float> PRICE =
        new Dictionary<int, float>
        {
                {(int)IDs.DEFAULT, 0},
                {(int)IDs.MOLECULE, 0}
        };

        public static readonly Dictionary<int, string> NAME =
        new Dictionary<int, string>
        {
                {(int)IDs.DEFAULT, "NO_NAME"},
                {(int)IDs.MOLECULE, "Molecule"}
        };

        public static readonly Dictionary<int, double> ATTRACTION =
        new Dictionary<int, double>
        {
                {(int)IDs.DEFAULT, 1},
                {(int)IDs.MOLECULE, 1}
        };

        public static readonly Dictionary<int, double> REPULSION =
        new Dictionary<int, double>
        {
                {(int)IDs.DEFAULT, 120},
                {(int)IDs.MOLECULE, 120}
        };
    }
}
