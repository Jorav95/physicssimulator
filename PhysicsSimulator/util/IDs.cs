﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhysicsSimulator
{
    public enum IDs
    {
        DEFAULT = 1,
        MOLECULE,
        #region Particles
        DEFAULT_PARTICLE,
        WRENCH,
        BOLT,
        MONEY,
        AFTERIMAGE,
        ALERTPARTICLE,
        DEATH,
        #endregion

        #region Music
        SONG1,
        SONG1INTRO,
        SONG3,    
        SONG2,
        VICTORY,
        GAMEOVER,
        PLAYERDEATHSOUND,
        SLOWMOSOUND,
        REVERSESLOWMOSOUND,
        PICKUPSOUND,
        TURBOENGINEPART,
        #endregion
    }
}
