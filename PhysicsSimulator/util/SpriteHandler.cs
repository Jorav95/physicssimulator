﻿
using PhysicsSimulator.objects;

namespace PhysicsSimulator
{
    class SpriteHandler
    {
        private const int ARRAYSIZE = 200;
        public static Sprite[] Sprites = new Sprite[ARRAYSIZE];
        public static Sprite GetSprite(int ID) {
            if(Sprites[ID] != null)
            {
                return new Sprite(Sprites[ID]);
            }
            else
            switch (ID)
            {
                    //case (int)IDs.MOLECULE: return new Sprite(Sprites[(int)IDs.RECTHULLPART]);

                    default: return new Sprite(Sprites[(int)IDs.DEFAULT]);
            }
        }
    }
}
