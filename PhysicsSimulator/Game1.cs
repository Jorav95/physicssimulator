﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using PhysicsSimulator.entity;
using PhysicsSimulator.entity.UI;
using PhysicsSimulator.objects;
using System.Collections.Generic;

namespace PhysicsSimulator
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        List<MoleculeEntity> controllers;
        Molecule testMolecule;
        Molecule testMolecule2;
        Molecule testMolecule3;
        Molecule testMolecule4;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = WindowSize.Width;
            graphics.PreferredBackBufferHeight = WindowSize.Height;
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            IsMouseVisible = true;
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            Texture2D baseTex = new Texture2D(GraphicsDevice, 2, 2);
            Color[] c = new Color[4];
            c[0] = Color.White;
            c[1] = Color.White;
            c[2] = Color.White;
            c[3] = Color.White;
            baseTex.SetData(c);
            Texture2DPlus baseTexPlus = new Texture2DPlus(baseTex);
            Sprite.addBaseTexture(baseTexPlus);

            Texture2DPlus moleculeTex = new Texture2DPlus(Content.Load<Texture2D>("textures/molecule"));
            Texture2DPlus errorTex = new Texture2DPlus(Content.Load<Texture2D>("textures/noTexture"));

            SpriteHandler.Sprites[(int)IDs.MOLECULE] = new Sprite(moleculeTex);
            SpriteHandler.Sprites[(int)IDs.DEFAULT] = new Sprite(errorTex);

            testMolecule = new Molecule(new Vector2(200, 200));
            testMolecule2 = new Molecule(new Vector2(300, 300));
            testMolecule3 = new Molecule(new Vector2(250, 200));
            testMolecule4 = new Molecule(new Vector2(225, 200));
            testMolecule.Mass *=3;
            controllers = new List<MoleculeEntity>();
            controllers.Add(new Neutral(testMolecule));
            controllers[0].Molecules.Add(testMolecule2);
            controllers[0].Molecules.Add(testMolecule3);
            controllers[0].Molecules.Add(testMolecule4);
            Molecule playerMolecule = new Molecule(new Vector2(graphics.PreferredBackBufferWidth / 2, graphics.PreferredBackBufferHeight / 2));
            MoleculeEntity player = new Player(playerMolecule);
            for (int i = 1; i <1000; i += 10)
            {
                player.Molecules.Add(new Molecule(new Vector2(graphics.PreferredBackBufferWidth / 2 + i, graphics.PreferredBackBufferHeight / 2 + i)));
            }
            controllers.Add(player);
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            foreach (MoleculeEntity player in controllers)
            {
                if(player is Player)
                player.Update(gameTime, controllers);
            }
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.LightGray);

            // TODO: Add your drawing code here
            spriteBatch.Begin();
            foreach (MoleculeEntity player in controllers)
            {
                if(player is Player)
                    (player as Player).Draw(spriteBatch, gameTime, controllers);
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
